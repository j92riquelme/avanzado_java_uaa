package concurrencia.sinsincronizacion;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Productor implements Runnable {

    private final Buffer datoCompartido;
    private final static Random generador = new Random();

    public Productor(Buffer datoCompartido) {
        this.datoCompartido = datoCompartido;
    }

    @Override
    public void run() {
        int suma = 0;
        for (int i = 1; i < 10; i++) {
            try {
                Thread.sleep(generador.nextInt(3000));
                suma = suma + i;
                datoCompartido.set(suma);

                System.out.println("El valor de la suma en Productor es: " + suma);
            } catch (InterruptedException ex) {
                Logger.getLogger(Productor.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.err.println("Productor finaliza tarea");
    }
}