package concurrencia.consincronizacion;

import java.util.Random;

public class Productor implements Runnable {

    private final static Random generador = new Random();
    private final Bufer ubicacionCompartida; // referencia al objeto compartido

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public Productor(Bufer compartido) {
        ubicacionCompartida = compartido;
    }

    @Override
    public void run() {
        int suma = 0;
        for (int cuenta = 1; cuenta <= 10; cuenta++) {
            try // permanece inactivo de 0 a 3 segundos, despues coloca valor en Bufer
            {
                Thread.sleep(generador.nextInt(3000));
                ubicacionCompartida.set(cuenta);
                suma += cuenta;
                System.out.printf(ANSI_BLUE + "Suma productor -> %d \n" + ANSI_RESET, suma);
            } catch (InterruptedException excepcion) {
                excepcion.printStackTrace();
            }
        }
        System.out.println("**********Productor termino de producir**********");
    }

}
