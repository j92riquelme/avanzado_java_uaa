package concurrencia.consincronizacion;

public class BuferConSincronizacion implements Bufer {
    private int bufer = -1;
    private boolean ocupado = false;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @Override
    public synchronized void set(int valor) throws InterruptedException {
        while (ocupado) {
            // imprime informacion del subproceso e informacion del bufer, despues espera
            System.out.printf(ANSI_RED + "\n+++Productor trata de escribir+++" + ANSI_RESET);
            System.out.printf(ANSI_RED + "\nBufer lleno. Productor espera."  + ANSI_RESET);
            wait();
        }
        bufer = valor;
        ocupado = true;

        mostrarEstado("->-> Productor escribe " + bufer);
        notifyAll();
    }

    @Override
    public synchronized int get() throws InterruptedException {
        // mientras no haya datos para leer, coloca el subproceso en el estado en espera
        while (!ocupado) {
            System.out.printf(ANSI_RED + "\n+++Consumidor trata de leer+++" + ANSI_RESET);
            System.out.printf(ANSI_RED + "\nBufer vacio. Consumidor espera." + ANSI_RESET);
            wait();
        }
        ocupado = false;

        mostrarEstado("->-> Consumidor lee " + bufer);

        notifyAll();
        return bufer;
    }

    public void mostrarEstado(String operacion) {
        System.out.printf("\n%s\n", operacion);
    }
}
