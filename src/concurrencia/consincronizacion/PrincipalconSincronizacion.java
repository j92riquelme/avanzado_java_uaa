package concurrencia.consincronizacion;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrincipalconSincronizacion {
    public static void main(String[] args) {
        ExecutorService aplicacion = Executors.newCachedThreadPool();
        Bufer ubicacionCompartida = new BuferConSincronizacion();

        System.out.printf("%s\t%s\n", "Operacion", "Bufer");
        System.out.printf("%s", "--------------------------------\n");

        // ejecuta las tareas Productor y Consumidor
        aplicacion.execute(new Productor(ubicacionCompartida));
        aplicacion.execute(new Consumidor(ubicacionCompartida));
        aplicacion.shutdown();
    }

}
