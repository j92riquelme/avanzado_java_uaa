package concurrencia.consincronizacion;

public interface Bufer {
    public void set(int valor) throws InterruptedException;

    public int get() throws InterruptedException;
}
