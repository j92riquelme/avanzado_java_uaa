package swing.tercerEjemplo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConeccionBaseDatos {
    
        private Connection baseDeDatos = null;
    private String driverBD = "org.postgresql.Driver";
    private String host = "jdbc:postgresql://localhost:5433/swing";
    private String user = "postgres";
    private String pass = "ROOT";

    public Connection conectarBD() throws SQLException {
        try {
            Class.forName(driverBD);

            baseDeDatos = DriverManager.getConnection(host, user, pass);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConeccionBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return baseDeDatos;
    }

    public void cerrarBD() throws SQLException {
        if (baseDeDatos != null) {
            baseDeDatos.close();
        }
    }

    public boolean insertarBD(String nombre, String tipo, Integer cantidad,String color,Integer talla,Integer precio) throws SQLException {
        String sentenciaSql = "INSERT INTO public.inventario(nombre, tipo, cantidad, color, talla, precio) VALUES (?, ?, ?, ?, ?, ?);";
        PreparedStatement st = baseDeDatos.prepareStatement(sentenciaSql);

        st.setString(1, nombre);
        st.setString(2, tipo);
        st.setInt(3, cantidad);
        st.setString(4, color);
        st.setInt(5, talla);
        st.setInt(6, precio);
        

        int cantidadRegistros = st.executeUpdate();
        st.close();

        if (cantidadRegistros == 0) {
            return false;
        }
        return true;
    }

    public ResultSet obtenerDatos(String sentenciaSql, String valores) throws SQLException {
        PreparedStatement st = baseDeDatos.prepareStatement(sentenciaSql);

        if (valores != null) {
            String[] valorObtenido = valores.split("-");

            for (int i = 0; i < valorObtenido.length; i++) {
                st.setString(i + 1, valorObtenido[i]);
            }

        }

        ResultSet resultado = st.executeQuery();
        //st.close();
        return resultado;
    }

    public void eliminarRegistro(Integer id) throws SQLException {
        String sql = "DELETE FROM public.inventario WHERE id_inventario=?;";
        PreparedStatement st = baseDeDatos.prepareStatement(sql);
        st.setInt(1, id);
        st.executeUpdate();
        st.close();
    }

    public boolean actualizarRegistro(String nombre, String tipo, Integer cantidad,String color,Integer talla,Integer precio, Integer id) throws SQLException {
        String sql = "UPDATE public.inventario SET nombre=?, tipo=?, cantidad=?, color=?, talla=?, precio=? WHERE id_inventario=?;";
        PreparedStatement st = baseDeDatos.prepareStatement(sql);

        st.setString(1, nombre);
        st.setString(2, tipo);
        st.setInt(3, cantidad);
        st.setString(4, color);
        st.setInt(5, talla);
        st.setInt(6, precio);
        st.setInt(7, id);

        int cantidadRegistros = st.executeUpdate();
        st.close();

        return cantidadRegistros != 0;

    }
    
    
}
