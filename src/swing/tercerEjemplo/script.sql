-- SEQUENCE: public.inventario_id_inventario_seq

-- DROP SEQUENCE IF EXISTS public.inventario_id_inventario_seq;

CREATE SEQUENCE IF NOT EXISTS public.inventario_id_inventario_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.inventario_id_inventario_seq
    OWNER TO postgres;

-- Table: public.inventario

-- DROP TABLE IF EXISTS public.inventario;

CREATE TABLE IF NOT EXISTS public.inventario
(
    id_inventario integer NOT NULL DEFAULT nextval('inventario_id_inventario_seq'::regclass),
    nombre character varying(50) COLLATE pg_catalog."default" NOT NULL,
    tipo character varying(50) COLLATE pg_catalog."default" NOT NULL,
    cantidad integer NOT NULL,
    color character varying(25) COLLATE pg_catalog."default" NOT NULL,
    talla integer NOT NULL,
    precio integer NOT NULL,
    CONSTRAINT inventario_pkey PRIMARY KEY (id_inventario)
    )
    WITH (
        OIDS = FALSE
        )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.inventario
    OWNER to postgres;

