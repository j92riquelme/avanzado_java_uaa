--DROP DATABASE swing;

CREATE DATABASE script.sql
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

--DROP SEQUENCE public.producto_cod_producto_seq;

CREATE SEQUENCE public.producto_cod_producto_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.producto_cod_producto_seq
    OWNER TO postgres;

--DROP TABLE public.producto;

CREATE TABLE IF NOT EXISTS public.producto
(
    cod_producto bigint NOT NULL DEFAULT nextval('producto_cod_producto_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL,
    fabricante character varying(100) COLLATE pg_catalog."default" NOT NULL,
    cantidad integer NOT NULL,
    CONSTRAINT producto_pkey PRIMARY KEY (cod_producto)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.producto
    OWNER to postgres;