package swing.segundoEjemplo;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BaseDeDatos {
    private Connection connection = null;
    private String driverBD = "org.postgresql.Driver";
    private String host = "jdbc:postgresql://localhost:5433/swing";
    private String user = "postgres";
    private String pass = "ROOT";

    public Connection conectarBD() throws SQLException {
        try {
            Class.forName(driverBD);
            connection = DriverManager.getConnection(host, user, pass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public boolean insertarBD(String producto, String fabricante, Integer cantidad) throws SQLException {
        String sql = "INSERT INTO public.producto(nombre, fabricante, cantidad)" +
                "	VALUES (?, ?, ?)";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, producto);
        st.setString(2, fabricante);
        st.setInt(3, cantidad);
        int nroRegistros = st.executeUpdate();
        st.close();
        if (nroRegistros == 0)
            return false;
        return true;
    }

    public ResultSet obtenerProductos() throws SQLException {
        String sql = "select cod_producto, nombre, fabricante, cantidad from producto";
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        //st.close();
        return rs;
    }


    public boolean eliminar(Integer id) throws SQLException {
        String sql = "delete from producto where cod_producto=?";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, id);
        int cantRegistros = st.executeUpdate();
        st.close();
        if (cantRegistros == 0)
            return false;
        return true;
    }

    public ResultSet buscarProductos(String descripcion, String fabricante, Integer cantidad) throws SQLException {
        String sql = "select cod_producto, nombre, fabricante, cantidad from producto where 1=1 ";
        if (descripcion != null)
            sql += " and nombre like ? ";
        if (fabricante != null)
            sql += " and fabricante like ? ";
        if (cantidad != null)
            sql += " and cantidad=? ";
        PreparedStatement st = connection.prepareStatement(sql);
        if (descripcion != null && fabricante == null && cantidad == null)
            st.setString(1, descripcion);
        if (descripcion == null && fabricante != null && cantidad == null)
            st.setString(1, fabricante);
        if (descripcion == null && fabricante == null && cantidad != null)
            st.setInt(1, cantidad);
        if (descripcion != null && fabricante != null && cantidad == null) {
            st.setString(1, descripcion);
            st.setString(2, fabricante);
        }
        if (descripcion != null && fabricante == null && cantidad != null) {
            st.setString(1, descripcion);
            st.setInt(2, cantidad);
        }
        if (descripcion == null && fabricante != null && cantidad != null) {
            st.setString(1, fabricante);
            st.setInt(2, cantidad);
        }
        if (descripcion != null && fabricante != null && cantidad != null) {
            st.setString(1, descripcion);
            st.setString(2, fabricante);
            st.setInt(3, cantidad);
        }
        ResultSet rs = st.executeQuery();
        //st.close();
        return rs;
    }


    public void cerrarBD() throws SQLException {
        if (connection != null)
            connection.close();
    }


}
