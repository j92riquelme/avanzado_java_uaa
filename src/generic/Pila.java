package generic;

public class Pila<E>
{
    private final int tamnho;
    private int superior;
    private E[] elementos;

    public Pila() {
        this(10);
    }

    public Pila(int s) {
        tamnho = s > 0 ? s : 10;
        superior = -1;
        elementos = (E[]) new Object[tamnho];
    }

    public void push(E valorIngresar) {

        if (superior == tamnho-1) {
            throw new ErrorPilaLlena(String.format("La pila esta llena, no se puede menter %s", valorIngresar));
        }
        elementos[++superior] = valorIngresar;

    }

    public E pop() {
        if (superior==-1) {
            throw new ErrorPilaVacia("La pila esta vacia, no se puede sacar");
        }
        return elementos[superior--];
    }

}
