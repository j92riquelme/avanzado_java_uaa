package generic;

public class Chocolate {

    private String marca;

    public Chocolate() {
    }

    public Chocolate(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
