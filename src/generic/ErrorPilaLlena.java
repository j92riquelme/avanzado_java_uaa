package generic;

public class ErrorPilaLlena extends RuntimeException
{

    private static final long serialVersionUID = 1L;

    public ErrorPilaLlena() {
        this("La pila esta llena");
    }

    public ErrorPilaLlena(String exp) {
        super(exp);
    }

}