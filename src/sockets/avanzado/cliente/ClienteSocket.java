package sockets.avanzado.cliente;

public class ClienteSocket {
	private static OperacionesCliente cliente;
	private static final int PUERTO_SERVER = 8081;
	private static final String HOST_SERVER = "localhost";

    public static void main(String[] args) {
       cliente = new OperacionesCliente(PUERTO_SERVER,HOST_SERVER);
    }
}
