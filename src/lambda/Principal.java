package lambda;

import java.util.ArrayList;
import java.util.List;

public class Principal {

    public static void main(String[] args) {
        List<Persona> listadoPer = new ArrayList<Persona>();
        listadoPer.add(new Persona("Javier", "Pereira", 37));
        listadoPer.add(new Persona("Lilian", "Mineur", 28));
        listadoPer.add(new Persona("Juan", "Gimenez", 15));
        listadoPer.add(new Persona("Isabella", "Gonzales", 25));
        listadoPer.add(new Persona("Jorge", "Rotela", 24));
        listadoPer.add(new Persona("Belen", "Benitez", 27));
        listadoPer.add(new Persona("Andres", "Lopez", 26));
        listadoPer.add(new Persona("Melisa", "Vera", 15));
        listadoPer.add(new Persona("Dario", "Duarte", 18));

        listadoPer.stream()
                .filter(p -> p.getEdad() > 25 || p.getApellido().charAt(0) == 'L')
                .map(p -> p.toString())
                .forEach(System.out::println);
    }
}