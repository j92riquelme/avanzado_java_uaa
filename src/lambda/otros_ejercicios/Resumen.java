package lambda.otros_ejercicios;

import lambda.Persona;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Resumen {

    public static void main(String[] args) {
        //https://www.oracle.com/lad/technical-resources/articles/java/expresiones-lambda-api-stream-java-part2.html

        /**
         * Stream
         */
        Stream orquestas = Stream.of("Grupo Niche", "Guayacán", "Son de Cali");

        /**
         * Stream
         */
        int[] enteros = new int[]{1, 2, 3, 4, 5};
        IntStream streamEnteros = Arrays.stream(enteros);

        /**
         * Stream
         */
        List<String> canciones = new ArrayList<>();
        canciones.add("Cancion a");
        canciones.add("Cancion b");
        canciones.add("Cancion c");
        Stream<String> streamCanciones = canciones.stream();

        /**
         * limit
         */
        Stream impares = Stream.iterate(1, x -> x + 2)
                .limit(10);

        /**
         * filter
         */
        List<String> ciudades = new ArrayList<>();
        //Stream de ciudades cuya primera letra es C de Cali
        Stream stream = ciudades.stream()
                .filter(s -> s.charAt(0) == 'C');

        /**
         * distinct
         */
        List<String> ciudadesList = Arrays.asList("Cali", "Bogotá", "Medellín", "Cali", "Bogotá", "Medellín", "Cali");
        //Stream sin ciudades repetidas: Cali, Bogotá,  Medellín
        Stream streamList = ciudadesList.stream()
                .distinct();

        /**
         * limit
         */
        List<String> ciudadesList2 = Arrays.asList("Cali", "Bogotá", "Medellín");
        //Stream limitado a los dos primeros elementos: Cali,  Bogotá
        Stream streamList2 = ciudadesList2.stream()
                .limit(2);

        /**
         * skip
         */
        List<String> ciudadesList3 = Arrays.asList("Cali", "Bogotá", "Medellín");
        //Stream que ha saltado los dos primeros elementos,  quedando solo: Medellín
        Stream streamList3 = ciudadesList3.stream()
                .skip(2);

        /**
         * map
         */
        List<String> paises = Arrays.asList("Colombia", "Perú", "Panamá");
        //Stream cuyos elementos son los países en mayúsculas
        Stream<String> streamPaises = paises.stream()
                .map(String::toUpperCase);

        /**
         * mapToDouble
         */
        List<Transaction> trxs = new ArrayList<>();
        //Stream de decimales cuyos elementos son el valor de  las transacciones
        DoubleStream streamtrxs = trxs.stream()
                .mapToDouble(Transaction::getValue);

        /**
         * mapToInt
         */
        List<String> paises2 = Arrays.asList("Colombia", "México", "Guatemala");
        //Stream de enteros cuyos elementos son el num de  caracteres de los países
        IntStream streampaises2 = paises2.stream()
                .mapToInt(String::length);

        /**
         * mapToDouble
         */
        List<Transaction> trxs2 = new ArrayList<>();
        //Stream de decimales cuyos elementos son el valor de  las transacciones
        DoubleStream streamtrxs2 = trxs2.stream()
                .mapToDouble(Transaction::getValue);

        /**
         * flatMap
         */
        List<String> lista = Arrays.asList("Taller", "Taller Lambdas y API  Stream");
        Stream streamlista = lista.stream()
                .map(s -> s.split(" ")) // Stream<String[]>
                .map(Arrays::stream) //  Stream<Stream<String>>
                .flatMap(Function.identity()) // Stream<String>
                .distinct(); //  Stream<String> de 5 elementos

        /**
         * flatMap
         */
        List<String> listaList = Arrays.asList("Taller", "Taller Lambdas y API  Stream");
        Stream streamList4 = listaList.stream()
                .map(s -> s.split(" ")) //  Stream<String[]>
                .flatMap(Arrays::stream)  // Stream<String>
                .distinct(); //  Stream<String> de 5 elementos

        /**
         * count
         */
        List<Transaction> trxslistaList = new ArrayList<>();
        long count = trxslistaList.stream()
                .filter(t -> t.getValue() > 2000)
                .count();

        /**
         * max
         */
        List<Transaction> trxsOp = new ArrayList<>();
        Optional<Transaction> max = trxsOp.stream()
                .max(Comparator.comparingDouble(Transaction::getValue));

        /**
         * allMatch
         */
        List<String> palabras = Arrays.asList("Java", "Lambdas", "Stream", "API");
        //Verifica si todas las palabras tienen un tamaño de 4  caracteres
        boolean longitud = palabras.stream()
                .allMatch(s -> s.length() >= 4);

        /**
         * anyMatch
         */
        List<String> palabrasDos = Arrays.asList("Java", "Lambdas",  "Stream", "API");
        //Verifica si existe la cadena “lambda” dentro del  Stream
        boolean anymatch = palabrasDos.stream()
                .anyMatch(s ->  s.equalsIgnoreCase("lambda"));

        /**
         * findAny
         */
        //Obtiene alguno de los elementos del Stream
        Optional<String>  alguno = palabras.stream()
                .findAny();

        /**
         * reduce
         */
        List<Integer> numeros = new ArrayList<>();
        //Obtiene el posible número mayor par del Stream
        Optional<Integer>  opt = numeros.stream()
                .filter(x -> x % 2 == 0)
                .reduce(Integer::max);

        /**
         * reduce
         */
        List<Integer> numerosDos = new ArrayList<>();
        //Obtiene la suma de los elementos del Stream
        Integer suma = numerosDos.stream()
                .reduce(0, (x,y) -> x + y);

        /**
         * collect
         */
        List<Integer> numerosTres = new ArrayList<>();
        //Cantidad de elementos en el Stream
        long  cuenta = numerosTres.stream()
                .collect(counting());

        /**
         * collect
         */
        List<Integer> numerosCuatro = new ArrayList<>();
        //Máximo elemento en el Stream de acuerdo al  comparador
        Optional<Integer> maximo = numerosCuatro.stream()
                .collect(maxBy(Comparator.naturalOrder()));

        /**
         * collect
         */
        List<Integer> numerosCinco = new ArrayList<>();
        //Obtiene la suma los elementos del Stream como un  entero
        int sumaCinco = numerosCinco.stream()
                .collect(summingInt(x -> x.intValue()));

        /**
         * collect
         */
        List<Integer>  numerosSeis = new ArrayList<>();
        IntSummaryStatistics res = numerosSeis.stream()
                .collect(summarizingInt(Integer::intValue));

        /**
         * collect
         */
        List<Integer>  numerosSiete = new ArrayList<>();
        String csv = numerosSiete.stream()
                .map(Object::toString)
                .collect(joining(", "));

        /**
         * collect
         */
        List<Persona> personas = new ArrayList<>();
        //  Agrupamiento
        Map<String, List<Persona>> porApe = personas.stream()
                .collect(groupingBy(Persona::getApellido));

        /**
         * collect
         */
        List<Persona> personasDos = new ArrayList<>();
        //  Agrupamiento
        Map<String, Long> deptCant = personasDos.stream()
                .collect(groupingBy(Persona::getApellido, counting()));


    }
}
