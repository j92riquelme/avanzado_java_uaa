package reflection;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;


public class App {

    public static void main(String[] args) {
        Class clase;
        Field constructor;

        try {
            clase = Class.forName("reflection.Persona");
            Field[] constructores = clase.getDeclaredFields();

            for (int i = 0; i < constructores.length; i++) {
                constructor = constructores[i];
                System.out.printf("Nombre de campo: %s \t Tipo de campo: %s %n",
                        constructor.getName(),
                        constructor.getType());
            }
        } catch (java.lang.NoClassDefFoundError ex) {
            System.err.println("NO EXISTE LA CLASE");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}