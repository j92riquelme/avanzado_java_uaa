package collections.parte2.set;

public class Prestamo implements Comparable<Prestamo> {
    protected Integer numeroPrestamo;
    private char tipoPrestamo;
    private int plazo;
    private int tasaAnual;
    private String moneda;
    private int montoCapital;
    private int montoInteres;
    private Integer saldoCapital;
    private int saldoInteres;
    private int cantidadCuotas;
    private int cuotasPagadas;
    private Cliente cliente;

    public Prestamo(Integer numeroPrestamo, char tipoPrestamo, int plazo, int tasaAnual, String moneda,
                    int montoCapital, int montoInteres, Integer saldoCapital, int saldoInteres, int cantidadCuotas,
                    int cuotasPagadas, Cliente cliente) {
        super();
        this.numeroPrestamo = numeroPrestamo;
        this.tipoPrestamo = tipoPrestamo;
        this.plazo = plazo;
        this.tasaAnual = tasaAnual;
        this.moneda = moneda;
        this.montoCapital = montoCapital;
        this.montoInteres = montoInteres;
        this.saldoCapital = saldoCapital;
        this.saldoInteres = saldoInteres;
        this.cantidadCuotas = cantidadCuotas;
        this.cuotasPagadas = cuotasPagadas;
        this.cliente = cliente;
    }

    public Prestamo() {
        super();
    }

    public Integer getNumeroPrestamo() {
        return numeroPrestamo;
    }

    public void setNumeroPrestamo(Integer numeroPrestamo) {
        this.numeroPrestamo = numeroPrestamo;
    }

    public char getTipoPrestamo() {
        return tipoPrestamo;
    }

    public void setTipoPrestamo(char tipoPrestamo) {
        this.tipoPrestamo = tipoPrestamo;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int getTasaAnual() {
        return tasaAnual;
    }

    public void setTasaAnual(int tasaAnual) {
        this.tasaAnual = tasaAnual;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public int getMontoCapital() {
        return montoCapital;
    }

    public void setMontoCapital(int montoCapital) {
        this.montoCapital = montoCapital;
    }

    public int getMontoInteres() {
        return montoInteres;
    }

    public void setMontoInteres(int montoInteres) {
        this.montoInteres = montoInteres;
    }

    public Integer getSaldoCapital() {
        return saldoCapital;
    }

    public void setSaldoCapital(Integer saldoCapital) {
        this.saldoCapital = saldoCapital;
    }

    public int getSaldoInteres() {
        return saldoInteres;
    }

    public void setSaldoInteres(int saldoInteres) {
        this.saldoInteres = saldoInteres;
    }

    public int getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(int cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public int getCuotasPagadas() {
        return cuotasPagadas;
    }

    public void setCuotasPagadas(int cuotasPagadas) {
        this.cuotasPagadas = cuotasPagadas;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "Prestamo [numeroPrestamo=" + numeroPrestamo + ", tipoPrestamo=" + tipoPrestamo + ", plazo=" + plazo
                + ", tasaAnual=" + tasaAnual + ", moneda=" + moneda + ", montoCapital=" + montoCapital
                + ", montoInteres=" + montoInteres + ", saldoCapital=" + saldoCapital + ", saldoInteres=" + saldoInteres
                + ", cantidadCuotas=" + cantidadCuotas + ", cuotasPagadas=" + cuotasPagadas + "\n cliente= \n" + cliente
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numeroPrestamo == null) ? 0 : numeroPrestamo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Prestamo other = (Prestamo) obj;
        if (numeroPrestamo == null) {
            if (other.numeroPrestamo != null)
                return false;
        } else if (!numeroPrestamo.equals(other.numeroPrestamo))
            return false;
        return true;
    }

    @Override
    public int compareTo(Prestamo p) {
        if (this.getSaldoCapital().intValue() < p.getSaldoCapital().intValue())
            return 1;
        else if (this.getSaldoCapital().intValue() > p.getSaldoCapital().intValue())
            return -1;
        else
            return 0;
    }

}
