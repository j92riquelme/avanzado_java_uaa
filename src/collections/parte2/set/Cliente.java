package collections.parte2.set;

public class Cliente implements Comparable<Cliente> {
    protected String numeroDocumento;
    private String nombre;
    private String apellido;
    private int edad;
    private char estadoCivil;
    private int telefono;
    private String email;

    public Cliente(String numeroDocumento, String nombre, String apellido, int edad, char estadoCivil, int telefono, String email) {
        super();
        this.numeroDocumento = numeroDocumento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.estadoCivil = estadoCivil;
        this.telefono = telefono;
        this.email = email;
    }

    public Cliente() {
        super();
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(char estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cliente [numeroDocumento=" + numeroDocumento + ", nombre=" + nombre + ", apellido=" + apellido
                + ", edad=" + edad + ", estadoCivil=" + estadoCivil + ", telefono=" + telefono + ", email=" + email
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cliente other = (Cliente) obj;
        if (numeroDocumento == null) {
            if (other.numeroDocumento != null)
                return false;
        } else if (!numeroDocumento.equals(other.numeroDocumento))
            return false;
        return true;
    }

    @Override
    public int compareTo(Cliente cl) {
        int compare = this.getNombre().compareTo(cl.getNombre());
        if (compare > 0)
            return 1;
        else if (compare < 0)
            return -1;
        else
            return 0;
    }

}
