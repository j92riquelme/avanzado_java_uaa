package collections.parte2.queue;

public class ActaNacimiento implements Comparable<ActaNacimiento> {

    private Integer numeroActa;
    private Integer numeroTomo;
    private String folio;
    private String nombreCompleto;
    private Character sexo;
    private Integer departamento;
    private Integer costoDocumento;

    public ActaNacimiento() {
    }

    public Integer getNumeroActa() {
        return numeroActa;
    }

    public void setNumeroActa(Integer numeroActa) {
        this.numeroActa = numeroActa;
    }

    public Integer getNumeroTomo() {
        return numeroTomo;
    }

    public void setNumeroTomo(Integer numeroTomo) {
        this.numeroTomo = numeroTomo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Integer getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Integer departamento) {
        this.departamento = departamento;
    }

    public Integer getCostoDocumento() {
        return costoDocumento;
    }

    public void setCostoDocumento(Integer costoDocumento) {
        this.costoDocumento = costoDocumento;
    }

    @Override
    public String toString() {
        return "acta: " + numeroActa +
                ", tomo: " + numeroTomo +
                ", folio: " + folio +
                ", nombre: " + nombreCompleto +
                ", sexo: " + sexo +
                ", departamento: " + departamento +
                ", costo: " + costoDocumento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ActaNacimiento))
            return false;

        ActaNacimiento that = (ActaNacimiento) o;

        if (!getNumeroActa().equals(that.getNumeroActa()))
            return false;
        if (!getNumeroTomo().equals(that.getNumeroTomo()))
            return false;
        return getFolio().equals(that.getFolio());
    }

    @Override
    public int hashCode() {
        int result = getNumeroActa().hashCode();
        result = 31 * result + getNumeroTomo().hashCode();
        return result;
    }

    @Override
    public int compareTo(ActaNacimiento o) {
        String actaActual = this.getNumeroActa().toString() + this.getNumeroTomo().toString() + this.getFolio();
        String acta = o.getNumeroActa().toString() + o.getNumeroTomo().toString() + o.getFolio();
        int result = actaActual.compareTo(acta);
        if (result < 0)
            return -1;
        else if (result > 0)
            return 1;
        return 0;
    }
}
