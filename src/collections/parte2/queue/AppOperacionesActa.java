package collections.parte2.queue;

import java.io.IOException;

public class AppOperacionesActa {

    public static void main(String[] args) {
        OperacionesActa operacionesActa = new OperacionesActa();
        try {
            operacionesActa.Menu();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
