package collections.parte2.queue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.SortedSet;
import java.util.TreeSet;

public class OperacionesActa {

    private PriorityQueue<ActaNacimiento> priorityActa = new PriorityQueue<>();
    private BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

    public void Menu() throws IOException {
        //Crear una aplicación que muestre el siguiente menú
        //1. Agregar curso
        //2. Mostrar cursos ordenados por cod. facultad desc
        //3. Modificar un curso
        //4. Eliminar un curso
        //5. Mostrar cursos ordenados por dia y turno asc
        //6. Salir
        int opcion = 0;
        while (opcion != 5) {
            System.out.println("Selecione una opcion");
            System.out.println("1. Agregar acta");
            System.out.println("2. Consultar actas");
            System.out.println("3. Modificar un acta");
            System.out.println("4. Eliminar un acta");
            System.out.println("5. Salir");
            System.out.println("Ingrese una opcion:");

            opcion = Integer.parseInt(entrada.readLine());

            switch (opcion) {
                case 1:
                    agregarActa();
                    break;
                case 2:
                    consultarActa();
                    break;
                case 3:
                    modificarActa();
                    break;
                case 4:
                    eliminarActa();
                    break;
                case 5:
                    System.out.println("Hasta luego");
                    break;
                default:
                    System.out.println("Opcion incorrecta");
            }
        }
    }

    public void agregarActa() throws IOException {
        //A la hora de agregar el programa se debe solicitar los datos por teclado e insertar en una colección.
        ActaNacimiento actaNacimiento = new ActaNacimiento();
        System.out.println("\n");

        System.out.println("Ingrese numero de acta:");
        actaNacimiento.setNumeroActa(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de tomo:");
        actaNacimiento.setNumeroTomo(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de folio:");
        actaNacimiento.setFolio(entrada.readLine());

        System.out.println("Ingrese nombre completo:");
        actaNacimiento.setNombreCompleto(entrada.readLine());

        System.out.println("Ingrese sexo:");
        actaNacimiento.setSexo(entrada.readLine().charAt(0));

        System.out.println("Ingrese numero de departamento:");
        actaNacimiento.setDepartamento(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese costo del documento:");
        actaNacimiento.setCostoDocumento(Integer.parseInt(entrada.readLine()));

        priorityActa.offer(actaNacimiento);
        System.out.println("\n***Acta agregada***");
    }

    public void consultarActa(){
        //A la hora de consultar se debe mostrar todos los datos almacenados en la colección.
        PriorityQueue<ActaNacimiento> priorityCopia = new PriorityQueue<>(priorityActa);

        System.out.println("\n*** Lista de Actas de nacimientos ***");
        while (!priorityCopia.isEmpty())
            System.out.println(priorityCopia.poll());
        System.out.println("\nCantidad:" + priorityActa.size());
        System.out.println("************************************************\n");
    }

    public void modificarActa() throws IOException {
        //A la hora de modificar se debe solicitar el numero de acta, tomo y folio,
        // verificar si existe un elemento con esos datos y actualizar el nombre, el sexo y el departamento.
        ActaNacimiento actaNacimiento = new ActaNacimiento();
        System.out.println("\n");

        System.out.println("Ingrese numero de acta:");
        actaNacimiento.setNumeroActa(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de tomo:");
        actaNacimiento.setNumeroTomo(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de folio:");
        actaNacimiento.setFolio(entrada.readLine());

        SortedSet<ActaNacimiento> sortedActa = new TreeSet<>(priorityActa);
        Iterator<ActaNacimiento> iteratorActa = sortedActa.iterator();
        while (iteratorActa.hasNext()){
            ActaNacimiento actaTemp = iteratorActa.next();
            if (actaTemp.equals(actaNacimiento)) {
                iteratorActa.remove();

                System.out.println("Ingrese nombre completo:");
                actaTemp.setNombreCompleto(entrada.readLine());

                System.out.println("Ingrese sexo:");
                actaTemp.setSexo(entrada.readLine().charAt(0));

                System.out.println("Ingrese numero de departamento:");
                actaTemp.setDepartamento(Integer.parseInt(entrada.readLine()));

                sortedActa.add(actaTemp);
                priorityActa.clear();
                priorityActa = new PriorityQueue<>(sortedActa);
                sortedActa.clear();
                System.out.println("\n***Acta de nacimiento actualizada correctamente***\n");
                return;
            }
        }
        System.out.println("\n***El acta de nacimiento no se encontro en la coleccion***\n");

    }

    public void eliminarActa() throws IOException {
        //A la hora de eliminar se debe solicitar el numero de acta, tomo, folio y departamento,
        // verificar si existe un elemento con esos datos y eliminar dicho elemento
        ActaNacimiento actaNacimiento = new ActaNacimiento();
        System.out.println("\n");

        System.out.println("Ingrese numero de acta:");
        actaNacimiento.setNumeroActa(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de tomo:");
        actaNacimiento.setNumeroTomo(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese numero de folio:");
        actaNacimiento.setFolio(entrada.readLine());

        System.out.println("Ingrese numero de departamento:");
        actaNacimiento.setDepartamento(Integer.parseInt(entrada.readLine()));

        SortedSet<ActaNacimiento> sortedActa = new TreeSet<>(priorityActa);
        Iterator<ActaNacimiento> iteratorActa = sortedActa.iterator();
        while (iteratorActa.hasNext()){
            ActaNacimiento actaTemp = iteratorActa.next();
            if (actaTemp.equals(actaNacimiento)
                    && actaTemp.getDepartamento().intValue() == actaNacimiento.getDepartamento().intValue()) {
                iteratorActa.remove();
                priorityActa.clear();
                priorityActa = new PriorityQueue<>(sortedActa);
                sortedActa.clear();
                System.out.println("\n***Acta de nacimiento eliminada correctamente***\n");
                return;
            }
        }
        System.out.println("\n***El acta de nacimiento no se encontro en la coleccion***\n");
    }

}
