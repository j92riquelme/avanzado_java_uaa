package collections.parte3.map;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalHashTable {

    public static void main(String[] args) {

        OperacionesProductoHashTable opp = new OperacionesProductoHashTable();
        try {
            opp.Menu();
        } catch (IOException ex) {
            Logger.getLogger(PrincipalHashTable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
